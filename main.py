import copy
import inspect
import json
import os
import re
import sys
import threading
import time
from collections import deque
from datetime import datetime, timedelta, timezone
from typing import List

import requests
import telebot
from bs4 import BeautifulSoup
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from telebot import apihelper
from telebot.apihelper import ApiException
from telebot.types import Message
from tzlocal import get_localzone

from config import TOKEN, admins, OWNER_ID, ADMIN_ID, ADMIN_NICK, URL_RULES, CHAT_ID, proxy
from logger import get_logger
from selenium_base import SeleniumBase


class ActivityChecker(SeleniumBase):
    bot = telebot.TeleBot(TOKEN)
    apihelper.proxy = proxy

    def __init__(self, nogui, skip_first_check=False):
        super().__init__(nogui)

        self.logger = get_logger()
        self.requests = deque()

        self.data_dir = 'data'
        self._init_need_dirs(dirs=[self.data_dir])

        self.file_vip_users = None
        self.file_data_to_interact = None
        self._init_need_files()

        self.number_post_to_check = 5
        self.number_post_skip_check = self.number_post_to_check if skip_first_check else 0

    def _init_need_files(self):
        """Создает файлы c начальными данными"""

        self.file_vip_users = os.path.join(self.data_dir, 'vip_users.json')
        self.file_data_to_interact = os.path.join(self.data_dir, 'data_to_interact.json')

        init_files_data = [
            (self.file_vip_users, {}),
            (self.file_data_to_interact, []),
        ]
        [self.dump_json(file, data) for file, data in init_files_data if not os.path.exists(file)]

    @staticmethod
    def dump_json(filename, data):
        """Записывает данные в json файл"""

        with open(filename, "w", encoding='utf-8') as file:
            file.write(json.dumps(data, ensure_ascii=False))

    @staticmethod
    def load_json(filename):
        """Считывает данные из json файла"""

        with open(filename, 'r', encoding='utf-8') as file:
            data = json.load(file)
        return data

    def make_report(self):
        """Формирует отчет о странице - скриншот и запись html в файл"""

        now_time = datetime.now()
        file_name = f'{now_time.strftime("%Y-%m-%d_%H-%M-%S")}'
        method_name = inspect.stack()[1][3]
        prefix_name = f'{method_name}_'

        self.dump_html(file_name, prefix=prefix_name)
        self.dump_screenshot(file_name, prefix=prefix_name)
        self.logger.info(f'Сформирован отчет')

    def command_ip(self, message: Message):
        """Вовращает данне об ip бота"""

        chat_id = message.chat.id
        if chat_id == OWNER_ID:
            j_response = requests.get('http://ip-api.com/json', proxies=apihelper.proxy).json()
            reply = (f"IP: {j_response.get('query')}\n"
                     f"Страна: {j_response.get('country')}\n"
                     f"Город: {j_response.get('city')}\n")
            self.bot.send_message(chat_id, reply)

    def command_add_vip(self, message: Message):
        """Добавление статуса вип пользователю"""

        spl_msg = message.text.split()

        if len(spl_msg) == 2:
            self.add_vip_user(spl_msg[1])

        elif len(spl_msg) == 3:
            try:
                days = int(spl_msg[2])
            except ValueError:
                return self.bot.send_message(message.chat.id, '2 аргумент должен быть целым цислом')
            else:
                self.add_vip_user(spl_msg[1], days)

        else:
            response = 'Данная команда должна иметь хотя бы один аргумент - никнейм'
            return self.bot.send_message(message.chat.id, response)

        self.command_show_vip(message)

    def command_show_vip(self, message: Message):
        """Обновляет и отправляет данные о текущих пользователях со статусом вип"""

        # проверка на случай, когда метод вызван из метода command_add_vip
        if message.text.startswith(self.get_command_code('show_vip')):
            spl_msg = message.text.split()
            sorted_username = True if len(spl_msg) == 2 and spl_msg[1] == 'n' else False
        else:
            sorted_username = False

        vip_users = self.get_clean_vip_users(sorted_username=sorted_username)

        reply = ''
        for user, date_end in vip_users.items():
            if sorted_username:
                reply += f'{user}: до {date_end}\n'
            else:
                reply += f'{date_end}: {user}\n'

        self.bot.send_message(message.chat.id, reply)

    def get_clean_vip_users(self, sorted_username: bool = False) -> dict:
        """
        Удаляет из файла пользователей, у которых истек статус вип.
        Вовращает актуальных список. По умолчанию список отсортирован по дате.

        **Args**

        ``sorted_username`` - True для сортировки по никнейму
        """

        vip_users: dict = self.load_json(self.file_vip_users)
        for user in list(vip_users.keys()):
            if not self.is_vip_user(user, vip_users=vip_users):
                vip_users.pop(user)

        if sorted_username:
            vip_users = dict(sorted(vip_users.items(), key=lambda item: item[0]))
        else:
            vip_users = dict(sorted(vip_users.items(), key=lambda item: item[1]))

        self.dump_json(self.file_vip_users, vip_users)
        return vip_users

    def alert_owner_and_exit(self, msg: str):
        """Отправляет сообщение об ошибке и завершает скрипт"""

        response = f'\U0000203CERROR! Я завершаю скрипт! \U0000203C \n {msg}\n'
        self.logger.critical('{!r}'.format(response))
        self.bot.send_message(OWNER_ID, response)
        sys.exit()

    def del_msg_and_alert_user(self, message: Message, tlg_name: str, alert_msg: str = None):
        """Удаляет сообщение и, если требуется, оповещает пользователя, почему удалено сообщение"""

        try:
            self.bot.delete_message(message.chat.id, message.message_id)
        except ApiException as error:
            self.logger.info(f'Сообщение уже было удалено вручную {error}')
            return

        if alert_msg is not None:
            self.bot.send_message(message.chat.id, f'{tlg_name}, {alert_msg}', parse_mode='HTML')
        self.logger.info('Удалено сообщение от {} - {!r}\n'.format(tlg_name, message.text))

    def add_vip_user(self, username, days=30):
        """Добавляет VIP статус пользователю"""

        data = self.load_json(self.file_vip_users)
        vip_end = str(datetime.now() + timedelta(days=days)).split('.')[0]
        data[username] = vip_end
        self.dump_json(self.file_vip_users, data)

        msg = f'Изменен VIP у {username}: до {vip_end}'
        self.logger.info(msg)
        return msg

    def get_message_for_interact(self,
                                 date_send_msg: int,
                                 last_hours=24,
                                 do_interact_certain_number=True) -> List[dict]:
        """
        Возвращает список данных для взаимодействия.
        Возможно получение постов либо за последние N часов, либо определенное кол-во из последних.

        **Args**:

        ``date_send_msg``: timestamp - дата отправления сообщения в телеграмм.
        Является точкой отсчета проверки за последние last_hours

        ``last_hours`` - кол-во часов, за которые нужно получить последние сообщения

        ``is_interact_certain_number`` - указывает на то, что взаимодействие нужно
        произвести с определенным кол-вом сообщений

        """

        date_send_msg = datetime.fromtimestamp(date_send_msg)
        data_to_interact: List[dict] = self.load_json(self.file_data_to_interact)
        response = []

        if data_to_interact:
            if do_interact_certain_number:
                response = data_to_interact[-self.number_post_to_check:]

            else:
                for data in data_to_interact:
                    date_post = datetime.strptime(data.get('date'), '%Y-%m-%d %H:%M:%S')
                    if date_send_msg - date_post < timedelta(hours=last_hours):
                        response.append(data)

        return response

    @staticmethod
    def get_telegram_name(message: Message):
        """Возвращает username в телеграме или, если нету, тогда имя"""

        if message.from_user.username:
            return f'@{message.from_user.username}'

        first_name = message.from_user.first_name
        last_name = message.from_user.last_name
        if first_name and last_name:
            return f'{first_name} {last_name}'
        return first_name or last_name

    def get_insta_name(self, text):
        """Возвращает username в инстаграмме"""

        soup = BeautifulSoup(text, 'html.parser')
        username_el = soup.select_one('article > header > div > div > div > h2 > a')

        if username_el is None:
            self.logger.critical('Не найден элемент username')
            self.make_report()
            return None

        username = username_el.text
        self.logger.info(f'Определен никнейм в instagram - {username}')
        return username

    @staticmethod
    def get_separate_url_msg(text: str):
        """
        Разделяет ссылку на инстаграмм, если она есть в сообщении,
        и сам текст сообщения, без ссылки

        :return: (url, purge_message)
        """

        result = re.findall(r'\bhttps://[w.]*instagram.com[^\s\n]*', text)
        if result:
            return result[0], text.replace(result[0], '')
        else:
            return None, text

    @staticmethod
    def is_admin_msg(message: Message) -> bool:
        """Проверяет является ли отправитель админом или ботом"""

        return message.from_user.id in admins or message.from_user.is_bot

    def is_vip_user(self, username: str, vip_users: dict = None) -> bool:
        """
        Проверяет является ли пользователь VIP.

        **Args**:

        ``vip_users``: dict - данные о вип пользователях. По умолчанию берутся из файла
         - key - никнейм
         - value - дата окончания vip
        """

        if vip_users is None:
            vip_users = self.load_json(self.file_vip_users)

        if username in vip_users.keys():
            vip_end = datetime.strptime(vip_users[username], '%Y-%m-%d %H:%M:%S')
            if vip_end > datetime.now():
                return True
        return False

    def is_valid_count_between_posts(self, user_id: int, between_posts=5) -> bool:
        """
        Проверка на необходимое кол-во чужих сообщений между повторными отправками поста

        **Args**:

        ``user_id``: - id пользователя, отправившего сообщение

        ``between_posts`` - необходимое кол-во постов между повторными публикациями
        """

        data_to_interact: List[dict] = self.load_json(self.file_data_to_interact)

        for msg in data_to_interact[-between_posts:]:
            if msg['user_id'] == user_id:
                self.logger.info('Пользователь недавно присылал уже свой пост')
                return False
        return True

    def is_valid_post_date(self, html_data, date_msg: int, hours_valid=30) -> bool:
        """
        Определяет на валидность правилам дату публикации поста

        **Args**:

        ``date_msg``: timestamp - дата сообщения в телеграмме

        ``html_data`` - html страница поста в инстаграмме

        ``hours_valid`` - допустимая разница между временем публикации поста
        и временем отправки поста в группу в часах
        """

        self.logger.info('Приступаю к проверке валидности даты поста в instagram')
        date_msg = datetime.fromtimestamp(date_msg).replace(tzinfo=get_localzone())

        soup = BeautifulSoup(html_data, 'html.parser')
        date_post_str = soup.select_one('time').get('datetime')
        date_post = datetime.strptime(date_post_str, '%Y-%m-%dT%H:%M:%S.000Z').replace(tzinfo=timezone.utc)

        if date_msg - date_post > timedelta(hours=hours_valid):
            self.logger.debug(f'Дата сообщения: {date_msg}')
            self.logger.debug(f'Дата поста: {date_post}')
            self.logger.info(f'Дата публикации поста не соотсветсвует условиям. Разница: {date_msg - date_post}\n')
            return False
        else:
            return True

    def listener(self, messages: List[Message]):
        """When new messages arrive TeleBot will call this function."""

        try:
            for m in messages:
                self.logger.debug(f"message -> {str(m)}")

                if m.content_type == 'left_chat_member':
                    self.bot.send_message(ADMIN_ID, f'Вышел пользователь: {self.get_telegram_name(m)}')
                    self.bot.delete_message(m.chat.id, m.message_id)
                    continue

                elif m.content_type == 'text':
                    self.logger.debug("{} (@{}) [{}]: {!r}\n".format(
                        m.from_user.first_name,
                        m.from_user.username,
                        m.from_user.id,
                        m.text,
                    ))

                    if m.chat.id == CHAT_ID:
                        self.requests.append(m)
                        continue

                    if self.is_admin_msg(m):
                        if m.text.startswith(self.get_command_code('ip')):
                            self.command_ip(m)

                        elif m.text.startswith(self.get_command_code('show_vip')):
                            self.command_show_vip(m)

                        elif m.text.startswith(self.get_command_code('add_vip')):
                            self.command_add_vip(m)

        except Exception as error:
            self.logger.exception(error)

    @staticmethod
    def get_alert_msg(msg_code: str,
                      tlg_name=None,
                      insta_name=None,
                      interact_url=None,
                      msg_text=None):
        """
        Возвращает сообщение согласно установленному коду. Префикс admin_ обозначает, что
        сообщение предназначается администратору или для логов.
        """

        msg_prefix = 'Ваше сообщение было удалено, '
        msg_suffix = f' Внимательнее читайте <a href="{URL_RULES}">правила</a> UPD 27.08.19.'

        alert_msgs = {
            'admin_dont_like': f'Не поставил лайк \n '
                               f'tlg_name = {tlg_name} \n '
                               f'insta_name = {insta_name} \n '
                               f'url = {interact_url}',

            'admin_dont_comment': f'Не оставил коммент \n '
                                  f'tlg_name = {tlg_name} \n '
                                  f'insta_name = {insta_name} \n '
                                  f'url = {interact_url}',

            'admin_vip_invalid': f'Пользователь отметил, что он VIP, но таких данных у меня нет, проверь! \n '
                                 f'tlg_name = {tlg_name} \n '
                                 f'insta_name = {insta_name} \n '
                                 f'{msg_text}',

            'date_invalid': f'{msg_prefix}'
                            'т.к. дата публикования поста не соответствует правилам чата.'
                            f'{msg_suffix}',

            'interact_invalid': f'{msg_prefix}'
                                f'т.к. Вы обошли не всех пользователей. '
                                f'Если не согласны пишите \U0001F449 @{ADMIN_NICK}.'
                                f'{msg_suffix}',

            'count_between_posts_invalid':  f'{msg_prefix}'
                                            'т.к. запрещено скидывать посты чаще, чем указано в правилах.'
                                            f'{msg_suffix}',

            'not_url': f'{msg_prefix}'
                       'т.к. не соответствует правилам чата.'
                       f'{msg_suffix}',
        }
        return alert_msgs.get(msg_code)

    @staticmethod
    def get_command_code(command: str):
        """Возвращает код команды"""

        command_codes = {
            'ip': '/ip',
            'show_vip': '/sv',
            'add_vip': '/av',
        }
        return command_codes.get(command)

    @staticmethod
    def get_code_to_interact(action: str):
        """Возвращает код действия"""

        actions = {
            'like': 'do_like',
            'comment': 'do_comment',
        }
        return actions.get(action)

    @staticmethod
    def msg_contains_vip(text: str):
        """Проверяет содержит ли сообщение пометку VIP"""

        vip_words = ['вип', 'vip']
        return any([word in text.lower() for word in vip_words])

    @staticmethod
    def msg_contains_like(text: str):
        """Проверяет содержит ли сообщение пометку лайк"""

        words = ['лайк', 'like']
        return any([word in text.lower() for word in words])

    @staticmethod
    def msg_contains_comment(text: str):
        """Проверяет содержит ли сообщение пометку комментарий"""

        words = ['comment', 'coment', 'коммент', 'комент']
        exclude = re.findall('ком.*желан', text.lower())
        if exclude:
            return False
        return any([word in text.lower() for word in words])

    def is_request_for_interact(self, message: Message) -> bool:
        """
        Проверяет является ли данный запрос валидным для обхода другими пользователями
        """

        is_admin_msg = self.is_admin_msg(message)
        tlg_name = self.get_telegram_name(message)
        url, purge_msg = self.get_separate_url_msg(message.text)

        # Проверка есть ли ссылка на инстаграмм в сообщение
        if url is None:
            if is_admin_msg:
                return False
            else:
                self.del_msg_and_alert_user(message, tlg_name, self.get_alert_msg('not_url'))
                return False

        # Проверка отправлено ли сообщение админом
        if is_admin_msg:
            self.logger.info('Пост для обхода отправлен админом\n')
            return True

        if self.number_post_skip_check > 0:
            self.number_post_skip_check -= 1
            self.logger.info(f"Задан пропуск проверки первых постов. Пропускаю еще {self.number_post_skip_check}")
            return True

        # Проверка на необходимое кол-во чужих сообщений между повторными отправками поста
        if not self.is_valid_count_between_posts(message.from_user.id):
            self.del_msg_and_alert_user(message, tlg_name, self.get_alert_msg('count_between_posts_invalid'))
            return False

        self.browser.get(url)
        insta_name = self.get_insta_name(self.browser.page_source)
        if insta_name is None:
            self.alert_owner_and_exit('Не найден элемент username!')

        # Проверка даты публикации поста на соответствие правилам
        if not self.is_valid_post_date(self.browser.page_source, message.date):
            self.del_msg_and_alert_user(message, tlg_name, self.get_alert_msg('date_invalid'))
            return False

        # Проверка есть ли пометка вип в сообщении и является ли пользователь VIP
        if self.msg_contains_vip(message.text):
            if not self.is_vip_user(insta_name):
                self.logger.info(f'Пользователь {insta_name} НЕ имеет VIP\n')
                self.bot.send_message(ADMIN_ID, self.get_alert_msg('admin_vip_invalid',
                                                                   tlg_name=tlg_name,
                                                                   insta_name=insta_name,
                                                                   msg_text=message.text))
            else:
                self.logger.info(f'Пользователь {insta_name} имеет VIP\n')
            return True

        self.wait_if_new_msg(date_msg=message.date)
        msg_to_interact = self.get_message_for_interact(date_send_msg=message.date)

        # Проверка на взаимодействие с другими пользователями согласно правилам
        for i, msg in enumerate(msg_to_interact, start=1):
            interact_url = msg.get('url')
            self.browser.get(interact_url)
            do_like = msg.get(self.get_code_to_interact('like'))
            do_comment = msg.get(self.get_code_to_interact('comment'))

            self.logger.info(f'[{i}/{len(msg_to_interact)}] '
                             f'Начинаю проверку действий для поста - {interact_url} '
                             f"like={do_like} "
                             f"comment={do_comment}")

            if msg.get('user_id') == message.from_user.id:
                self.logger.info('Это его же пост, пропускаю.')
                continue

            # Проверка на взаимодействие комментарием
            if do_comment:
                time.sleep(2)
                valid_comment = self.validate_comment(insta_name)

                if not valid_comment:
                    if valid_comment is None:
                        self.alert_owner_and_exit('Не найден элемент comment!')

                    self.del_msg_and_alert_user(message, tlg_name, self.get_alert_msg('interact_invalid'))
                    log_msg = self.get_alert_msg('admin_dont_comment',
                                                 tlg_name=tlg_name,
                                                 insta_name=insta_name,
                                                 interact_url=interact_url)
                    self.logger.info('{!r}\n'.format(log_msg))
                    self.bot.send_message(ADMIN_ID, log_msg)

                    return False

            # Проверка на взаимодействие лайком
            if do_like:
                time.sleep(2)
                validate_like = self.validate_like(insta_name)

                if not validate_like:
                    if validate_like is None:
                        self.alert_owner_and_exit('Не найден элемент like!')

                    self.del_msg_and_alert_user(message, tlg_name, self.get_alert_msg('interact_invalid'))
                    log_msg = self.get_alert_msg('admin_dont_like',
                                                 tlg_name=tlg_name,
                                                 insta_name=insta_name,
                                                 interact_url=interact_url)
                    self.logger.info('{!r}\n'.format(log_msg))
                    self.bot.send_message(ADMIN_ID, log_msg)

                    return False

        self.logger.info(f'Участник выполнил все условия - {insta_name}\n')
        return True

    def validate_like(self, insta_name, interact_url=None, scroll_dialog=100):
        """
        Проверяет поставил ли пользователь лайк на пост

        **Args**:

        ``insta_name`` - никнейм, который проверяется

        ``interact_url`` - url поста, с которым нужно проверить взаимодействие.
        По умолчанию считается, что страница уже подготовлена(открыта) на предыдущих этапах

        ``scroll_dialog`` - сколько раз пытаться прокрутить всплывающее диалоговое окно с лайками
        """

        if interact_url is not None:
            self.browser.get(interact_url)

        soup = BeautifulSoup(self.browser.page_source, 'html.parser')
        video_post = soup.select_one('.vcOH2')
        if video_post:
            self.logger.info('Это видео пост, пропускаю проверку лайков')
            return True

        # Клик по кнопке для открытия диалогового окна с лайкнувшими пользователями
        try:
            self._web_click('._8A5w5', times=3)
            dialog_xpath = '/html/body/div[4]/div/div[2]/div'
            self._web_wait(xpath=dialog_xpath)
            dialog = self.browser.find_element_by_xpath(dialog_xpath)

        except Exception as error:
            if not self.is_page_available():
                self.logger.info('Пост удален')
                return True

            self.logger.critical(f'Не найден элемент на странице {error}', exc_info=True)
            self.make_report()
            return True

        users_liked_el = []

        # прокрутить несколько раз диалоговое окно вниз
        for i in range(scroll_dialog):
            time.sleep(2 if i % 10 != 0 else 7)
            soup = BeautifulSoup(self.browser.page_source, 'html.parser')
            users_liked_el_new = soup.select('._7UhW9 a')
            if users_liked_el_new == users_liked_el:
                break

            users_liked_el.clear()
            users_liked_el.extend(users_liked_el_new)
            if any([insta_name == user.get('title') for user in users_liked_el]):
                return True

            self._web_scroll_down(dialog)

        return False

    def validate_comment(self, insta_name, interact_url=None, show_more=30, try_num=1):
        """
        Проверяет оставил ли пользователь комментарий под постом

        **Args**:

        ``insta_name`` - никнейм, который проверяется

        ``interact_url`` - url поста, с которым нужно проверить взаимодействие.
        По умолчанию считается, что страница уже подготовлена(открыта) на предыдущих этапах

        ``show_more`` - сколько раз пытаться нажать на "показать больше комментариев"

        ``try_num`` - номер попытки повторного поиска элемента при его отсутствии (для рекурсии)
        """

        if interact_url is not None:
            self.browser.get(interact_url)

        time.sleep(3)
        comment_selector = 'ul h3'
        soup = BeautifulSoup(self.browser.page_source, 'html.parser')
        try:
            self._web_wait(comment_selector, timeout=10)
        except TimeoutException:
            pass

        comments_el = soup.select(comment_selector)

        if not comments_el:
            if not self.is_page_available():
                self.logger.info('Пост удален')
                return True

            if try_num < 3:
                self.logger.info(f'Не найден элемент, пробую найти заново, try_num={try_num}')
                time.sleep(60)
                try_num += 1
                self.browser.delete_all_cookies()
                self.browser.get(self.browser.current_url)
                return self.validate_comment(insta_name, try_num=try_num)

            self.logger.critical('Не найден элемент comments!')
            self.make_report()
            return None

        for _ in range(show_more):
            try:
                if any([insta_name == comment.select_one('a').get('title') for comment in comments_el]):
                    return True
            except (AttributeError, TypeError):
                pass

            # нажать на кнопку "показать больше комментариев"
            try:
                self._web_click('article > div > div > ul > li button', times=3)
                time.sleep(1)
            except NoSuchElementException:
                break
            else:
                soup = BeautifulSoup(self.browser.page_source, 'html.parser')
                comments_el = soup.select(comment_selector)

        return False

    def start(self):
        """Запуск бота"""

        self.bot.set_update_listener(self.listener)
        self.bot.send_message(OWNER_ID, 'Я запущен заново')

        threading.Thread(name='Checker', target=self.start_check).start()

        while True:
            try:
                self.bot.polling(none_stop=True, interval=0, timeout=60)

            except Exception as error:
                self.logger.error(error)
                time.sleep(5 * 60)

            else:
                # сработает, если polling остановить вручную
                self._virtual_display_stop()
                break

    def start_check(self):
        """Инициализация проверки сообщений"""

        while True:
            try:
                if self.requests:
                    self.browser = self._set_browser()

                    for request in copy.deepcopy(self.requests):
                        if self.is_request_for_interact(request):
                            actions = self.definition_actions(request)
                            self.write_request_for_interact(request, actions)

                        self.requests.popleft()
                else:
                    time.sleep(7)

            except SystemExit:
                self.bot.stop_polling()
                self.logger.error('Инициализирован stop_polling! Я завершаю программу.\n')
                break

            except KeyboardInterrupt:
                self._virtual_display_stop()

            except Exception as error:
                self.logger.exception(error)
                time.sleep(10 * 60)

            finally:
                self._browser_quit()

    def definition_actions(self, message: Message) -> dict:
        """Определяет какие взаимодействия хочет получить пользователь"""

        purge_text = self.get_separate_url_msg(message.text)[1]
        do_like = self.msg_contains_like(purge_text)
        do_comment = self.msg_contains_comment(purge_text)

        return {
            self.get_code_to_interact('like'): do_like,
            self.get_code_to_interact('comment'): do_comment,
        }

    def wait_if_new_msg(self, date_msg: int, minutes=3):
        """
        Если сообщение опубликовано недавно, то подождать N минут

        **Args**:

        ``date_msg``: timestamp - дата отправления сообщения в телеграмме

        ``minutes`` - кол-во минут, после которых сообщение уже не считается новым
        """

        time_passed = datetime.now().timestamp() - date_msg
        time_post_is_new = minutes * 60

        if time_passed < time_post_is_new:
            time_wait = time_post_is_new - time_passed
            self.logger.info(f'Сообщение слишком свежее, подожду {round(time_wait/60)} минут')
            time.sleep(time_wait)

    def write_request_for_interact(self, message: Message, actions: dict):
        """Записывает в файл данные о сообщении, которое требует обхода другими пользователями"""

        data: List[dict] = self.load_json(self.file_data_to_interact)
        data.append({
            'msg_id': message.message_id,
            'user_id': message.from_user.id,
            'date': str(datetime.fromtimestamp(message.date)),
            'url': self.get_separate_url_msg(message.text)[0],
            **actions,
        })
        self.dump_json(self.file_data_to_interact, data)


if __name__ == '__main__':
    ActivityChecker(nogui=True, skip_first_check=True).start()
