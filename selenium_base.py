import os
import sys
from datetime import datetime
from time import sleep
from typing import List

try:
    from pyvirtualdisplay import Display
except ModuleNotFoundError:
    pass

from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class SeleniumBase:
    def __init__(self, nogui, browser=None):
        self.browser = browser
        self.nogui = nogui
        path = os.path.dirname(os.path.abspath(__file__))
        self.webriver_path = os.path.join(path, 'assets', 'chromedriver')

        self.tmp_dir = 'tmp'
        self.dump_html_dir = self.tmp_dir
        self.dump_screenshot_dir = self.tmp_dir
        self._init_need_dirs(dirs=[self.tmp_dir])

        if self.nogui:
            if not sys.platform.startswith('win32'):
                self.display = Display(visible=0, size=(1680, 1080))
                self.display.start()
            else:
                self.display = None

    @staticmethod
    def _init_need_dirs(dirs: List):
        """Создает необходимые для работы директории"""

        [os.mkdir(dir_) for dir_ in dirs if not os.path.exists(dir_)]

    def _web_click(self, selector: str, times=True):
        """
        Находит элемент и совершает клик по нему

        **Args**:

        ``times``: True or int - количество раз сколько будет пытаться нажать на элемент. True - бесконечно.
        """

        selector_el = self.browser.find_element_by_css_selector(selector)

        while times:
            try:
                selector_el = self.browser.find_element_by_css_selector(selector)
                selector_el.click()
                sleep(1)
                self.browser.execute_script("window.scrollTo(0,0);")
                break

            except:
                sleep(1)
                try:
                    self.browser.execute_script("arguments[0].scrollIntoView();", selector_el)
                except StaleElementReferenceException:
                    break

            if times is not True:
                times -= 1

    def _web_wait(self, selector=None, xpath=None, timeout=10):
        """Ожидание подгрузки данных по селектору на странице"""

        if selector:
            WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

        elif xpath:
            WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((By.XPATH, xpath)))

    def _web_scroll_by_height(self, times=3, height=200):
        """Прокручивает на заданную высоту необходимое кол-во раз"""

        while times:
            self.browser.execute_script(f"window.scrollTo(0, {height});")
            times -= 1

    def _web_scroll_down(self, element, times=1):
        """Прокручивает всплывающее диалоговое окно заданное кол-во раз"""

        while times:
            self.browser.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", element)
            sleep(1)
            times -= 1

    def _virtual_display_stop(self):
        """Закрывает виртуальный дисплей, если он существует"""

        if self.nogui:
            if isinstance(self.display, Display):
                self.display.stop()

    def _set_browser(self):
        """Устанавливает настройки браузера"""

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--window-size=900,900')

        if self.nogui:
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--disable-gpu')

        return webdriver.Chrome(self.webriver_path, options=chrome_options)

    def _browser_quit(self):
        """Завершает работу браузера, если он определен"""

        if getattr(self.browser, 'quit', None):
            self.browser.quit()

    def is_page_available(self):
        """Проверяет доступна ли страница и валидна ли"""

        expected_keywords = ["Page Not Found", "Content Unavailable"]

        if any(keyword in self.browser.title for keyword in expected_keywords):
            self.browser.refresh()
            sleep(3)

            if any(keyword in self.browser.title for keyword in expected_keywords):
                return False

        return True

    def dump_screenshot(self, filename=None, prefix: str = '', suffix: str = ''):
        """
        Получение скриншота со страницы

        **Args**:

        ``filename``:
            Название файла. Если не указано, то берется в формате Y-m-d_H-M-S

        ``prefix``:
            Префикс названия файла, если необходимо

        ``suffix``:
            Суффикс названия файла, если необходимо
        """

        if filename is None:
            now_time = datetime.now()
            filename = f'{now_time.strftime("%Y-%m-%d_%H-%M-%S")}'

        filename_full = f'{prefix}{filename}{suffix}.png'
        path_name = os.path.join(self.dump_screenshot_dir, filename_full)
        self.browser.get_screenshot_as_file(f'{path_name}')

    def dump_html(self, filename=None, prefix: str = '', suffix: str = ''):
        """
        Запись html страницы в файл

         **Args**:

        ``filename``:
            Название файла. Если не указано, то берется в формате Y-m-d_H-M-S

        ``prefix``:
            Префикс названия файла, если необходимо

        ``suffix``:
            Суффикс названия файла, если необходимо
        """

        if filename is None:
            now_time = datetime.now()
            filename = f'{now_time.strftime("%Y-%m-%d_%H-%M-%S")}'

        filename_full = f'{prefix}{filename}{suffix}.html'
        path_name = os.path.join(self.dump_html_dir, filename_full)
        with open(f'{path_name}', "w", encoding='utf-8') as file:
            file.write(str(self.browser.page_source))
