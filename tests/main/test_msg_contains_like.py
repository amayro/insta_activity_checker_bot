import pytest


@pytest.fixture
def template_messages_with_like():
    """Список вариантов сообщений содержащие пометку like"""

    return [
        'сообщение лаЙк\nздесь',
        'likE',
    ]


@pytest.fixture
def template_messages_without_like():
    """Список вариантов сообщений без пометки like"""

    return [
        'л1айк',
    ]


def test_msg_contains_like_valid(ac, template_messages_with_like):
    for msg in template_messages_with_like:
        assert ac.msg_contains_like(msg) is True


def test_msg_contains_like_invalid(ac, template_messages_without_like):
    for msg in template_messages_without_like:
        assert ac.msg_contains_like(msg) is False
