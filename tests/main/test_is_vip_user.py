import pytest


@pytest.fixture
def data_vip_users():
    return {
        "nick": "3010-05-01 11:16:02",
        "jack": "2010-05-01 11:16:02"
    }


def test_is_vip_user_valid(ac, data_vip_users):
    assert ac.is_vip_user('nick', data_vip_users) is True


def test_is_vip_user_invalid(ac, data_vip_users):
    assert ac.is_vip_user('jack', data_vip_users) is False
    assert ac.is_vip_user('sara', data_vip_users) is False
