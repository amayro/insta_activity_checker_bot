import os

import pytest
from main import ActivityChecker


@pytest.fixture(scope='session')
def path_data():
    """Возвращает путь к данным для тестов"""

    path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(path, 'data')


@pytest.fixture(scope='session')
def ac(path_data):
    """Экземпляр класса чекера без ведения логов и с измененными путями для данных"""

    _ac = ActivityChecker(nogui=True)
    _ac.logger.setLevel(100)

    _ac.file_vip_users = os.path.join(path_data, 'vip_users.json')
    _ac.file_data_to_interact = os.path.join(path_data, 'data_to_interact.json')

    return _ac


