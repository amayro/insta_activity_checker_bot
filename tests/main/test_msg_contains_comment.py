import pytest


@pytest.fixture
def template_messages_with_comment():
    """Список вариантов сообщений содержащие пометку comment"""

    return [
        'сообщение комментарий\nздесь',
        'комент\nпоставьте',
        'лайк по желанию \nкомент\nпоставьте',
    ]


@pytest.fixture
def template_messages_without_comment():
    """Список вариантов сообщений без пометки comment.
    Если есть пометка 'по желанию', то считается, что пометкой comment можно принебречь"""

    return [
        'лайк коментарий по желанию',
        'лайк комментарий по желанию',
        'лайк коммент по желанию',
        'лайк комент по желанию',
        'лайк комент ( по желанию )',
        'лайк комментарий (по желанию)',
        'привет',
    ]


def test_msg_contains_comment_valid(ac, template_messages_with_comment):
    for msg in template_messages_with_comment:
        assert ac.msg_contains_comment(msg) is True


def test_msg_contains_comment_invalid(ac, template_messages_without_comment):
    for msg in template_messages_without_comment:
        assert ac.msg_contains_comment(msg) is False
