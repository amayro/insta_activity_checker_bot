import os

import mock
import pytest


@pytest.fixture
def url_with_valid_user(path_data):
    """
    Ссылка на посты с валидными комментариями в виде кортежа, где
    1 элемент - ссылка на пост,
    2 элемент - никнейм пользователя, оставившего комментарий
    """

    return [
        (os.path.join(path_data, 'liketime.spb.html'), 'nataligrassman'),
        ('https://www.instagram.com/p/BwWqW-cAGJ0/', 'macelle_patrick'),
        ('https://www.instagram.com/p/Bv1Hn9cAT1w/', 'macelle_patrick'),
        ('https://www.instagram.com/p/BxfyojRIrAc/?igshid=1ozepjd9fczhu', '_________'),  # удаленный пост
    ]


@pytest.fixture
def url_with_invalid_user(path_data):
    """
    Ссылка на посты с НЕвалидными комментариями в виде кортежа, где
    1 элемент - ссылка на пост,
    2 элемент - никнейм пользователя, НЕоставившего комментарий
    """

    return [
        (os.path.join(path_data, 'liketime.spb.html'), '_________'),
    ]


@pytest.fixture
def url_invalid(path_data):
    return [
        os.path.join(path_data, 'none.html'),
    ]


@pytest.fixture(scope='module', autouse=True)
def setup(ac):
    ac.browser = ac._set_browser()


def test_validate_comment_valid(ac, url_with_valid_user):
    for url, user in url_with_valid_user:
        assert ac.validate_comment(user, url) is True


def test_validate_comment_invalid(ac, url_with_invalid_user):
    for url, user in url_with_invalid_user:
        assert ac.validate_comment(user, url) is False


@mock.patch('main.ActivityChecker.make_report')
def test_validate_comment_invalid_selector(mocked_make_report, ac, url_invalid):
    """Если страница не валидна или если нету на странице искомого элемента с заданным селектором"""

    for url in url_invalid:
        assert ac.validate_comment('_________', url) is None
        mocked_make_report.assert_called_once_with()
