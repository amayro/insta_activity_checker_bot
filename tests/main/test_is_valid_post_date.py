import os
import pytest
from datetime import datetime


@pytest.fixture
def data_html():
    filename = os.path.join('tests', 'data', 'liketime.spb.html')
    with open(filename, 'r', encoding='utf-8') as file:
        return file.read()


@pytest.fixture
def dates_msg_valid():
    dates = [
        '2019-04-20 15:45:02',
        '2019-04-21 15:45:02',
    ]
    return [datetime.strptime(date, '%Y-%m-%d %H:%M:%S').timestamp() for date in dates]


@pytest.fixture
def dates_msg_invalid():
    dates = [
        '2019-04-21 23:45:02',
        '2019-04-23 23:45:02',
    ]
    return [datetime.strptime(date, '%Y-%m-%d %H:%M:%S').timestamp() for date in dates]


def test_is_valid_post_date_valid(ac, data_html, dates_msg_valid):
    for date in dates_msg_valid:
        assert ac.is_valid_post_date(data_html, date) is True


def test_is_valid_post_date_invalid(ac, data_html, dates_msg_invalid):
    for date in dates_msg_invalid:
        assert ac.is_valid_post_date(data_html, date) is False
