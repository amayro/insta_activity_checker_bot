import pytest


@pytest.fixture
def template_messages():
    """
    Список вариантов сообщений в виде кортежей, где
    1 элемент это само сообщение,
    2 - ожидаемый url,
    3 - ожидаемый текст
    """

    return [
        (
            'https://www.instagram.com/p/Bwl_8FL32/ лайк, спасибо 😉😉',
            'https://www.instagram.com/p/Bwl_8FL32/',
            ' лайк, спасибо 😉😉'
        ),

        (
            '❤️🎈https://instagram.com/p/Bwl_8FL32/ лайк, спасибо',
            'https://instagram.com/p/Bwl_8FL32/',
            '❤️🎈 лайк, спасибо'
        ),

        (
            'https://www.instagram.com/p/Bwov\nлайк, спасибо 😉😉',
            'https://www.instagram.com/p/Bwov',
            '\nлайк, спасибо 😉😉',
        ),

        (
            'лайк https://www.instagram.com/p/Bwov спасибо',
            'https://www.instagram.com/p/Bwov',
            'лайк  спасибо',
        ),

        (
            'Лайк\nКоммент(спасибо)\n🌸🌸🌸\nhttps://www.instagram.com/alenaa.vi/p/BwmpYx7BsQr/',
            'https://www.instagram.com/alenaa.vi/p/BwmpYx7BsQr/',
            'Лайк\nКоммент(спасибо)\n🌸🌸🌸\n'
        ),

        (
            'Лайк\nКоммент(спасибо)\n🌸🌸🌸\nhttps:/www.yandex.ru/BwmpYx7BsQr/xtr9j',
            None,
            'Лайк\nКоммент(спасибо)\n🌸🌸🌸\nhttps:/www.yandex.ru/BwmpYx7BsQr/xtr9j'
        ),
    ]


def test_get_separate_url_msg(ac, template_messages):
    for msg, url_expect, msg_expect in template_messages:
        assert ac.get_separate_url_msg(msg) == (url_expect, msg_expect)
