import os

import mock
import pytest


@pytest.fixture
def scroll_dialog():
    """сколько раз пытаться прокрутить всплывающее диалоговое окно с лайками"""
    return 10


@pytest.fixture(scope='module')
def url_with_valid_user(path_data):
    """Ссылки на инстаграм в виде кортежа, где 2 элемент - никнейм лайкнувшего пользователя"""

    return [
        ('https://www.instagram.com/p/BXR8nAYFBks/', 'flowridge'),
        ('https://www.instagram.com/p/BXR8nAYFBks/', 'kiki_sgr'),
        ('https://www.instagram.com/p/BwWqW-cAGJ0/', 'any_nameXXXXXXXXXXXXXX'),  # ссылка с видео постом
    ]


@pytest.fixture
def urls_with_no_like_user(path_data):
    """Ссылки на инстаграм в виде кортежа, где 2 элемент - никнейм НЕлайкнувшего пользователя"""

    return [
        ('https://www.instagram.com/p/BfZSI-WlwEb/', 'any_nameXXXXXXXXXXXXXX'),
    ]


@pytest.fixture
def url_invalid(path_data):
    return os.path.join(path_data, 'none.html')


@pytest.fixture(scope='module', autouse=True)
def setup(ac):
    ac.browser = ac._set_browser()


def test_validate_like_valid(ac,
                             url_with_valid_user,
                             scroll_dialog):
    """
    Тест на валидность лайков.
    Если тест не прошел, тогда сначала проверить вручную ссылку из тестов!
    """

    for url, user in url_with_valid_user:
        ac.browser.get(url)
        assert ac.validate_like(user, url, scroll_dialog) is True


def test_validate_like_invalid(ac,
                               urls_with_no_like_user,
                               scroll_dialog):

    for url, user in urls_with_no_like_user:
        ac.browser.get(url)
        assert ac.validate_like(user, url, scroll_dialog) is False


@mock.patch('main.ActivityChecker.make_report')
def test_validate_like_invalid_selector(mocked_make_report, ac, url_invalid, scroll_dialog):
    """Если страница не валидна или если нету на странице искомого элемента с заданным селектором"""

    assert ac.validate_like('any_user_name', url_invalid, scroll_dialog) is True
    mocked_make_report.assert_called_once_with()
