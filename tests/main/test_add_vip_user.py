from datetime import datetime, timedelta


def test_add_vip_user_valid(ac):
    username = 'nick'

    vip_end_0 = str(datetime.now()).split('.')[0]
    vip_end_2 = str(datetime.now() + timedelta(days=2)).split('.')[0]
    vip_end_30 = str(datetime.now() + timedelta(days=30)).split('.')[0]

    assert ac.add_vip_user(username, 0) == f'Изменен VIP у {username}: до {vip_end_0}'
    assert ac.add_vip_user(username, 2) == f'Изменен VIP у {username}: до {vip_end_2}'
    assert ac.add_vip_user(username) == f'Изменен VIP у {username}: до {vip_end_30}'
