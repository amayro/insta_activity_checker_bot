import os

import mock
import pytest


@pytest.fixture
def data_html():
    filename = os.path.join('tests', 'data', 'liketime.spb.html')
    with open(filename, 'r', encoding='utf-8') as file:
        return file.read()


def test_get_insta_name_valid(ac, data_html):
    assert ac.get_insta_name(data_html) == 'liketime.spb'


@mock.patch('main.ActivityChecker.make_report')
def test_get_insta_name_invalid(mocked_make_report, ac):
    assert ac.get_insta_name('') is None
    mocked_make_report.assert_called_once_with()
