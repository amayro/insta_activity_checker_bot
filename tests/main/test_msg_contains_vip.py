import pytest


@pytest.fixture
def template_messages_with_vip():
    """Список вариантов сообщений содержащие вип"""

    return [
        'xzcВИПdfs',
        'viP\nфпкнр ырер',
        'httpsВИП',
    ]


@pytest.fixture
def template_messages_without_vip():
    """Список вариантов сообщений без вип"""

    return [
        'лайк',
    ]


def test_msg_contains_vip_valid(ac, template_messages_with_vip):
    for msg in template_messages_with_vip:
        assert ac.msg_contains_vip(msg) is True


def test_msg_contains_vip_invalid(ac, template_messages_without_vip):
    for msg in template_messages_without_vip:
        assert ac.msg_contains_vip(msg) is False
